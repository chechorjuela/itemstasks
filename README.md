    Desde un sistema operativo Linux
    #Agregar host
    sudo gedit /etc/hosts
    
    Agregar linea 
    127.0.0.1	itemtask.dev.com
    Guardar y salir
    
    #Ejecutar comando para agregar un virtual Host
     sudo gedit /etc/apache2/sites-available/itemtask.dev.com.config
    
    
    <VirtualHost *:80>
    	ServerAdmin webmaster@localhost
    	ServerName shopping.dev.com
    	DocumentRoot /var/www/html/itemstasks/public
    	<Directory />
    		Options FollowSymLinks
    		AllowOverride All
    	</Directory>
    	<Directory /var/www/html/itemstasks/public/>
    		Options Indexes FollowSymLinks MultiViews
    		AllowOverride All
    		Order allow,deny
    		allow from all
    	</Directory>
    	ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
    	<Directory "/usr/lib/cgi-bin">
    		AllowOverride None
    		Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
    		Order allow,deny
    		Allow from all
    	</Directory>
    	ErrorLog ${APACHE_LOG_DIR}/error.log
    	# Possible values include: debug, info, notice, warn, error, crit,
    	# alert, emerg.
    	LogLevel warn
    	CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost> 
    
    
    Guardar y Salir
    
    sudo a2ensite shopping.dev.com.conf 
    
    Actualizar apache
    sudo service apache2 reload
    
    entrar al directorio raiz de la carpeta del proyecto y ejecutar el comando 
    composer install
    
    Cambiar permisos a los directorios bootstrap y cache
    sudo chmod 775 -R storage/ boostrap/cache
    
    Cambiar grupo del directorio
    cd ..
    sudo chgrp www-data -R NombreDeLaCarpetaDelProyecto
    
    
    Importar 
    La base de datos desde db/Allitems.sql
    
    instalar los npm de angular
    cd web/clientApi
    
    npm install 
    
    ng serve 
    
    #por defecto el cliente esta apuntando a un servidor "https://itemstasks.herokuapp.com/"
    #al igual que la parte del backend
    #Si desea correrlo desde la parte local cambiar la configuracion de backend .env 
    .env
    
    DB_CONNECTION=mysql
    DB_HOST=localhost
    DB_PORT=3306
    DB_DATABASE=database
    DB_USERNAME=root
    DB_PASSWORD=secrect
    
    #y en el archivo web/clientApi/src/app/config/app.settings.ts en urlApi cambiar la ruta del local
    public urlApi: string = "{https://itemstasks.herokuapp.com/api/}";
    
    Postman:
    https://www.getpostman.com/collections/424187a82d7536d12202