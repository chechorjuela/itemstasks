export class UserSignUpContract {
  firstname:string;
  lastname:string;
  role?:string;
  email:string;
  password:string;
  username:string;
}
