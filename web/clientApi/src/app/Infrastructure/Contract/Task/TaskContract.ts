export class TaskContract {
  id:number;
  name:string;
  priority:number;
  date_expiration:string;
  user_from:number;
  created_at:Date;
  updated_at:Date;
}
