import {Injectable} from "@angular/core";
import {ResponseApiModel} from "../../../Domain/Models/Common/ResponseApiModel";
import {TaskModel} from "../../../Domain/Models/Task/TaskModel";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AppSettings} from "../../../config/app.settings";
import {LocalStorageUtilsService} from "../../../Domain/Helpers/Utils/LocalStorageUtils.Service";
import {TokenContract} from "../../Contract/Auth/TokenContract";
import {TaskContract} from "../../Contract/Task/TaskContract";
import {ResponseData} from "../../../Domain/Models/Common/ResponseData";


@Injectable()
export abstract class ITaskRepository {
  abstract getAllItemsPaginate(page:number):Promise<ResponseApiModel<ResponseData<TaskModel>>>;
  abstract saveItemsTask(task:TaskContract):Promise<ResponseApiModel<TaskModel>>;
  abstract deleteItemTask(taskId:number):Promise<ResponseApiModel<TaskModel>>;
  abstract updateItemTask(task:TaskContract,id:number):Promise<ResponseApiModel<TaskModel>>;
}

@Injectable()
export class TaskRepository implements ITaskRepository{
  private baseUrl = new AppSettings().urlApi;
  private taskUrl = new AppSettings().urls.urlTask;
  private singUpUrl = new AppSettings().urls.signUpUrl;
  private token: TokenContract;
  public headersBasic = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': null });
  constructor(
    private http:HttpClient,
    private localStorageUtils:LocalStorageUtilsService<TokenContract>,
    private appSettings:AppSettings
  ){
    this.baseUrl = appSettings.urlApi;
    this.token = this.localStorageUtils.getObject(appSettings.userKeyStore);

  }

  getAllItemsPaginate(page:number): Promise<ResponseApiModel<ResponseData<TaskModel>>> {
    this.headersBasic.set("Authorization",'bearer '+this.token.token);
    let headersoptions = new HttpHeaders().set('Authorization','bearer '+this.token.token);
    // @ts-ignore
    return this.http.get<ResponseApiModel<ResponseData>>(this.baseUrl + this.taskUrl+"?page="+page,{headers:headersoptions})
      .toPromise()
      .then(response => {

        return response;
      })
      .catch(error => {
      });
  }

  saveItemsTask(task: TaskContract): Promise<ResponseApiModel<TaskModel>> {
    this.headersBasic.set("Authorization",'bearer '+this.token.token);
    let headersoptions = new HttpHeaders().set('Authorization','bearer '+this.token.token);

    return this.http.post<ResponseApiModel<TaskModel>>(this.baseUrl + this.taskUrl,task,{headers:headersoptions})
      .toPromise()
      .then(resp=>{
        return resp
      });
  }

  deleteItemTask(taskId: number): Promise<ResponseApiModel<TaskModel>> {
    let headersoptions = new HttpHeaders().set('Authorization','bearer '+this.token.token);
    // @ts-ignore
    return this.http.delete(this.baseUrl+this.taskUrl+"/"+taskId,{headers:headersoptions})
      .toPromise()
      .then(response=>{
        return response;
      }).catch(error=>{

      })
  }

  updateItemTask(task: TaskContract,id:number): Promise<ResponseApiModel<TaskModel>> {
    let headersoptions = new HttpHeaders().set('Authorization','bearer '+this.token.token);
    // @ts-ignore
    return this.http.put(this.baseUrl+this.taskUrl+'/'+id,task,{headers:headersoptions})
      .toPromise()
      .then(response=>{
        return response;
      }).catch(error=>{

      });
  }

}
