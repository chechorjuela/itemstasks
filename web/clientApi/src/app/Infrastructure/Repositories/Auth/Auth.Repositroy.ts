import {Injectable} from "@angular/core";
import {SigninContract} from "../../Contract/Auth/SigninContract";
import {AppSettings} from "../../../config/app.settings";
import {HttpClient,HttpRequest,HttpHeaders} from "@angular/common/http";
import {ResponseApiModel} from "../../../Domain/Models/Common/ResponseApiModel";
import {AuthModel} from "../../../Domain/Models/Auth/Auth.Model";
import {UserSignUpContract} from "../../Contract/User/UserSignUpContract";
import {UserModel} from "../../../Domain/Models/Users/UserModel";
import {LocalStorageUtilsService} from "../../../Domain/Helpers/Utils/LocalStorageUtils.Service";
import {TokenContract} from "../../Contract/Auth/TokenContract";

@Injectable()
export abstract class IAuthRepository {
  abstract SignIn(signin:SigninContract):Promise<ResponseApiModel<AuthModel>>;
  abstract SignUp(signup:UserSignUpContract):Promise<ResponseApiModel<UserModel>>;
  abstract GetUSer():Promise<ResponseApiModel<AuthModel>>;
  abstract Logout();
}

@Injectable()
export class AuthRepositroy implements IAuthRepository{
  private token:TokenContract;
  private baseUrl = new AppSettings().urlApi;
  private signinUrl = new AppSettings().urls.signInUrl;
  private singUpUrl = new AppSettings().urls.signUpUrl;
  private getUser = new AppSettings().urls.getUser;
  private headersBasic = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': null });

  constructor(
    private http:HttpClient,
    private appSettings:AppSettings,
    private localStorageUtils:LocalStorageUtilsService<TokenContract>,
    ){
    this.baseUrl = appSettings.urlApi;
    this.token = this.localStorageUtils.getObject(appSettings.userKeyStore);

  }

  Logout() {

  }

  SignIn(signin:SigninContract):Promise<ResponseApiModel<AuthModel>> {
    // @ts-ignore
    return this.http.post<ResponseApiModel<AuthModel>>(this.baseUrl + this.signinUrl, signin)
      .toPromise()
      .then(response => {
        //let responseApi:ResponseApiModel<AuthModel> = new ResponseApiModel<AuthModel>();

        return response;
      })
      .catch(error => {
      });
  }

  SignUp(signup: UserSignUpContract): Promise<ResponseApiModel<UserModel>> {
    // @ts-ignore
    return this.http.post<ResponseApiModel<UserModel>>(this.baseUrl + this.singUpUrl, signup)
      .toPromise()
      .then(response => {
        return response;
      })
      .catch(error => {
      });
  }

  GetUSer():Promise<ResponseApiModel<AuthModel>>{
    this.token = this.localStorageUtils.getObject(this.appSettings.userKeyStore);
    let headersoptions = new HttpHeaders().set('Authorization','bearer '+this.token.token);
    // @ts-ignore
    return this.http.get<ResponseApiModel<AuthModel>>(this.baseUrl + this.getUser,{headers:headersoptions})
      .toPromise()
      .then(resp=>{
        return resp;
      }).catch(onreject=>{
        return onreject;
      })
  }


}
