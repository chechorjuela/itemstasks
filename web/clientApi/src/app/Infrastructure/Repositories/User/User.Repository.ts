import {Injectable} from "@angular/core";
import {ResponseApiModel} from "../../../Domain/Models/Common/ResponseApiModel";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AppSettings} from "../../../config/app.settings";
import {LocalStorageUtilsService} from "../../../Domain/Helpers/Utils/LocalStorageUtils.Service";
import {TokenContract} from "../../Contract/Auth/TokenContract";
import {ResponseData} from "../../../Domain/Models/Common/ResponseData";
import {UserModel} from "../../../Domain/Models/Users/UserModel";


@Injectable()
export abstract class IUserRepository {
  abstract getAllUser():Promise<ResponseApiModel<ResponseData<UserModel>>>;

}

@Injectable()
export class UserRepository implements IUserRepository{
  private baseUrl = new AppSettings().urlApi;
  private userUrl = new AppSettings().urls.urlUser;
  private token: TokenContract;
  public headersBasic = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': null });
  constructor(
    private http:HttpClient,
    private localStorageUtils:LocalStorageUtilsService<TokenContract>,
    private appSettings:AppSettings
  ){
    this.baseUrl = appSettings.urlApi;
    this.token = this.localStorageUtils.getObject(appSettings.userKeyStore);

  }

  getAllUser(): Promise<ResponseApiModel<ResponseData<UserModel>>> {
    this.headersBasic.set("Authorization",'bearer '+this.token.token);
    let headersoptions = new HttpHeaders().set('Authorization','bearer '+this.token.token);
    // @ts-ignore
    return this.http.get<ResponseApiModel<ResponseData>>(this.baseUrl + this.userUrl,{headers:headersoptions})
      .toPromise()
      .then(response => {

        return response;
      })
      .catch(error => {
      });
  }

}
