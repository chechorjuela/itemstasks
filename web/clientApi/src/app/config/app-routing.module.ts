import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdminRoutes} from "../Application/Concepts/Admin/Admin.Routes";
import {AuthRoutes} from "../Application/Concepts/Auth/Auth.Routes";
import {RootComponent} from "../Application/Concepts/Root/Components/root.component";

const routes: Routes = [
  ...AdminRoutes,
  ...AuthRoutes,
  {path: '', component: RootComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
