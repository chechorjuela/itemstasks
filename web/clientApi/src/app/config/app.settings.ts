export class AppSettings {
  public urlApi: string = "https://itemstasks.herokuapp.com/api/";
  public urls = {
    signInUrl: 'auth/signin',
    signUpUrl: 'auth/signup',
    getUser: 'auth/profile',
    urlTask: 'task',
    urlUser: 'user',
  };
  public statusResponse = {
    ok: 'success',
    fail: 'failed'
  };

  public userKeyStore: string = "UserInfoStorage"
}
