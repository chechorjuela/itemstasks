import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ModalDialogModule} from 'ngx-modal-dialog';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
/**
 * Components
 */
import {MaterializeComponentModule} from "../Domain/Helpers/Modules/MaterializeComponentModule";
import {AppComponent} from '../Application/Concepts/Root/Components/app.component';
import {SigninComponent} from "../Application/Concepts/Auth/Componets/Singin/Signin.component";
import {SignupComponent} from "../Application/Concepts/Auth/Componets/Signup/Signup.component";
import {TasksComponent} from "../Application/Concepts/Admin/Components/Tasks/Tasks.component";
import {UserComponent} from "../Application/Concepts/Admin/Components/Users/User.component";
import {ProfileComponent} from "../Application/Concepts/Admin/Components/Profile/Profile.component";
import {TaskFormComponent} from "../Application/Concepts/Admin/Components/Tasks/Modals/TaskForm.component";
import {AppSettings} from "./app.settings";
/**
 * Repository
 */
import {AuthRepositroy, IAuthRepository} from "../Infrastructure/Repositories/Auth/Auth.Repositroy";
import {AuthService, IAuthService} from "../Domain/Services/Auth/Auth.Service";
import {LocalStorageUtilsService} from "../Domain/Helpers/Utils/LocalStorageUtils.Service";
import {ITaskRepository, TaskRepository} from "../Infrastructure/Repositories/Task/Task.Repository";
import {ITaskService, TaskService} from "../Domain/Services/Task/Task.Service";
import {TaskComunicationService} from "../Domain/Services/Task/TaskComunication.service";
import {AuthGuard} from "../Application/Concepts/Auth/Componets/AuthGuard";
import {AuthComunicationService} from "../Domain/Services/Auth/AuthComunicationService";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {IUserRepository, UserRepository} from "../Infrastructure/Repositories/User/User.Repository";
import {IUserService, UserService} from "../Domain/Services/User/User.Service";
import {RootComponent} from "../Application/Concepts/Root/Components/root.component";

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    TasksComponent,
    UserComponent,
    ProfileComponent,
    TaskFormComponent,
    RootComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MaterializeComponentModule.forRoot(),
    ModalDialogModule.forRoot(),
  ],
  entryComponents: [
    TaskFormComponent
  ],
  providers: [
    AppSettings,
    LocalStorageUtilsService,
    TaskComunicationService,
    AuthComunicationService,
    AuthGuard,
    {provide: IAuthRepository, useClass: AuthRepositroy},
    {provide: IAuthService, useClass: AuthService},
    {provide: ITaskRepository, useClass: TaskRepository},
    {provide: ITaskService, useClass: TaskService},
    {provide: IUserRepository, useClass: UserRepository},
    {provide: IUserService, useClass: UserService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
