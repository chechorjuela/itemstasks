import {Routes} from "@angular/router";
import {TasksComponent} from "./Components/Tasks/Tasks.component";
import {UserComponent} from "./Components/Users/User.component";
import {ProfileComponent} from "./Components/Profile/Profile.component";
import {AuthGuard} from "../Auth/Componets/AuthGuard";

export const AdminRoutes: Routes = [
  {path: 'admin/tasks', component: TasksComponent, canActivate: [AuthGuard]},
  {path: 'admin/users', component: UserComponent, canActivate: [AuthGuard]},
  {path: 'admin/profile', component: ProfileComponent, canActivate: [AuthGuard]}
];
