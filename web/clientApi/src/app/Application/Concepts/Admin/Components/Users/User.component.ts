import {Component, OnInit} from "@angular/core";
import {UserService} from "../../../../../Domain/Services/User/User.Service";
import {UserModel} from "../../../../../Domain/Models/Users/UserModel";

@Component({
  providers:[UserService],
  templateUrl:'../../View/Users/User.component.html',
  selector:'users-app'
})
export class UserComponent implements OnInit{
  listUser:UserModel[]=null;
  constructor(private userService:UserService) {

  }

  ngOnInit(): void {
    this.userService.getAllUsers().then(resp=>{
      this.listUser = resp.Data.data
    })
  }

}
