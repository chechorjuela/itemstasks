import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MzBaseModal, MzModalComponent, MzToastService} from 'ngx-materialize';
import {TaskContract} from '../../../../../../Infrastructure/Contract/Task/TaskContract';
import {TaskService} from '../../../../../../Domain/Services/Task/Task.Service';
import {TaskComunicationService} from '../../../../../../Domain/Services/Task/TaskComunication.service';
import {Subscription} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from "../../../../../../Domain/Services/User/User.Service";
import {UserModel} from "../../../../../../Domain/Models/Users/UserModel";
import {LocalStorageUtilsService} from "../../../../../../Domain/Helpers/Utils/LocalStorageUtils.Service";
import {TokenContract} from "../../../../../../Infrastructure/Contract/Auth/TokenContract";

@Component({
  selector: 'app-modal-form',
  providers: [TaskService, UserService],
  styleUrls: ['../../../Css/Tasks/task.css'],
  templateUrl: '../../../View/Tasks/Modals/TashForm.component.html'
})
export class TaskFormComponent extends MzBaseModal implements OnInit, OnDestroy {

  selectableUser: UserModel[] = [];
  subscriptionTaskSelect: Subscription;
  titleModal: string = "Add Task";
  userForm: FormGroup;
  currentUser:UserModel;
  public loading = false;
  public taskContractInput: TaskContract = new TaskContract();
  errorMessageResources = {
    name: {
      required: 'Name is required'
    }
  };
  @Input() taskSend: TaskContract;
  @ViewChild('modalUser') modalUser: MzModalComponent;
  modalOptions: Materialize.ModalOptions = {
    dismissible: true, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '15%', // Ending top style attribute
    ready: (modal, trigger) => { // Callback for Modal open. Modal and trigger parameters available.
    },
    complete: () => console.info('Closed'), // Callback for Modal close
  };

  constructor(
    private taskService: TaskService,
    private userService: UserService,
    private taskCommunicationService: TaskComunicationService,
    private formBuilder: FormBuilder,
    private toastService: MzToastService,
    private localStoreUtils:LocalStorageUtilsService<UserModel>

  ) {
    super();
    this.subscriptionTaskSelect = this.taskCommunicationService.sendAnnounceTaskEdit$.subscribe(
      task => {
        this.titleModal = "Edit Task";
        this.taskContractInput = task;
      }
    );
    this.loadingCurrentUser();
    this.loadingUser();
    this.buildForm();
  }

  buildForm() {
    this.userForm = this.formBuilder.group({
      name: [this.taskContractInput.name, Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50)
      ])],
      priority: [this.taskContractInput.priority, Validators.compose([])],
      date_expiration: [this.taskContractInput.date_expiration, Validators.compose([
        Validators.required,
      ])],
      user_from: [this.taskContractInput.user_from, Validators.compose([
        Validators.required,
      ])]
    });
  }

  public datepickerOptions: Pickadate.DateOptions = {
    clear: 'Clear', // Clear button text
    close: 'Ok',    // Ok button text
    today: 'Today', // Today button text
    closeOnClear: true,
    closeOnSelect: true,
    format: 'yyyy/mm/dd', // Visible date format (defaulted to formatSubmit if provided otherwise 'd mmmm, yyyy')
    formatSubmit: 'yyyy/mm/dd',   // Return value format (used to set/get value)
    onClose: () => console.info('Close has been invoked.'),
    onOpen: () => console.info('Open has been invoked.'),
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 10,    // Creates a dropdown of 10 years to control year,
  };

  ngOnInit() {
    this.taskContractInput.name = '';
    this.taskContractInput.date_expiration = null;
    this.taskContractInput.priority = 0;

    if (this.taskSend != null) {
      this.taskContractInput = this.taskSend;
      this.userForm.setValue({
        'name': this.taskSend.name,
        'priority': this.taskSend.priority,
        'date_expiration': this.taskSend.date_expiration
      });
    }
  }

  ngOnDestroy(): void {
    this.subscriptionTaskSelect.unsubscribe();
  }
  loadingCurrentUser(){
    this.currentUser = this.localStoreUtils.getObject('UserInfoStorage');
  }
  loadingUser() {
    this.userService.getAllUsers().then(resp => {
      if (resp.Header.status == "success") {
        this.selectableUser = resp.Data.data;
      }
    });
  }

  saveTask(e) {
    if (this.userForm.valid) {
      if (typeof this.userForm.value.priority == 'undefined') {
        this.userForm.value.priority = 0;
      } else {
        this.userForm.value.priority = 1;
      }
      if (this.taskSend == null) {
        this.taskService.saveItemsTask(this.userForm.value).then(data => {
          if (data.Header.status == 'success') {
            this.modalUser.closeModal();
            this.taskCommunicationService.annouceTaskPush(data.Data);
          }
        });
      } else {
        this.taskService.updateItemTask(this.userForm.value, this.taskSend.id).then(data => {
          if (data.Header.status == 'success') {
            this.modalUser.closeModal();
            this.taskCommunicationService.annouceTaskPush(data.Data);
          }
        });
      }
    } else {
      this.toastService.show("Datos incorrectos", 4000, 'red', () => console.info("mostrar"));
    }
  }
}
