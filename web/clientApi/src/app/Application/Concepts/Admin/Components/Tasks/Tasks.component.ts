import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, ViewContainerRef} from '@angular/core';
import {MzModalComponent, MzModalService, MzToastService} from 'ngx-materialize';
import {TaskFormComponent} from './Modals/TaskForm.component';
import {TaskService} from '../../../../../Domain/Services/Task/Task.Service';
import {AppSettings} from '../../../../../config/app.settings';
import {TaskModel} from '../../../../../Domain/Models/Task/TaskModel';
import {TaskContract} from '../../../../../Infrastructure/Contract/Task/TaskContract';
import {TaskComunicationService} from '../../../../../Domain/Services/Task/TaskComunication.service';

import {ModalDialogService, SimpleModalComponent} from 'ngx-modal-dialog';
import {Subscription} from 'rxjs';

@Component({
  providers: [TaskService],
  templateUrl: '../../View/Tasks/Tasks.component.html',
  selector: 'task-app',
})

export class TasksComponent implements OnInit, OnDestroy {


  taskList: Array<TaskModel> = new Array<TaskModel>();
  taskListToExpire: Array<TaskModel> = new Array<TaskModel>();
  taskListExpire: Array<TaskModel> = new Array<TaskModel>();
  loadingTask = true;
  //@Output() taskEmmiter = new EventEmitter<TaskContract>();
  taskSelected: TaskContract = new TaskContract();
  public currentPage = 1;
  public itemsPerPage: number;
  public totalItems: number;
  subscriptionTaskPush: Subscription;

  constructor(
    private modalService: MzModalService,
    private taskService: TaskService,
    private appSettings: AppSettings,
    private toastService: MzToastService,
    private modalServiceDialog: ModalDialogService,
    private viewRef: ViewContainerRef,
    private taskCommunicationService: TaskComunicationService,
  ) {

    this.subscriptionTaskPush = taskCommunicationService.sendAnnounceTaskPush$.subscribe(
      taskPush => {
        this.taskList.push(taskPush);
        this.loadingPage();
      }
    );
  }

  ngOnDestroy() {
    this.subscriptionTaskPush.unsubscribe();
    this.taskList=null;
  }

  ngOnInit(): void {
    this.loadingPage();
  }

  deleteTask(event, task) {
    event.preventDefault();
    const taskSelect: TaskContract = task;
    this.modalServiceDialog.openDialog(this.viewRef, {
      title: 'Delete',
      childComponent: SimpleModalComponent,
      data: {
        text: 'Are You sure You want to delete this?'
      },
      placeOnTop: true,
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [{
        text: 'Yes',
        buttonClass: 'btn btn-danger',
        onAction: () => new Promise((resolve: any, reject: any) => {
          // this.deleteTaskService(taskSelect.id);
          this.taskService.deleteItemTask(taskSelect.id).then(data => {
            if (data.Header.status == this.appSettings.statusResponse.ok) {
              const index = this.taskList.findIndex(t => t.id == taskSelect.id);
              if (index > -1) {
                this.taskList.splice(index, 1);
              }
              this.toastService.show(data.Header.message, 4000, 'green', () => console.info(data.Header.message));
              resolve();

            } else {
              this.toastService.show('Ups! hubo un error', 4000, 'red', () => console.info('Ups! hubo un error'));
              resolve();
            }
          });
          // resolve();
        })
      }, {
        text: 'no',
        buttonClass: 'btn btn-default',
        onAction: () => new Promise((resolve: any) => {
          resolve();
        })
      }]
    });
  }

  changePage(e) {
    switch (e.target.innerText) {
      case 'chevron_right':
        if ((this.totalItems / this.itemsPerPage) > this.currentPage) {
          this.currentPage = this.currentPage + 1;
          this.loadingPage();
        }
        break;
      case 'chevron_left':
        if (this.currentPage > 1) {
          this.currentPage = this.currentPage - 1;
          this.loadingPage();
        }
        break;
      case 'first_page':
        if (this.currentPage != 1) {
          this.currentPage = 1;
          this.loadingPage();
        }
        break;
      case 'last_page':
        if ((this.totalItems / this.itemsPerPage) != this.currentPage) {
          let page = this.totalItems / this.itemsPerPage > 1 ? (this.totalItems % this.itemsPerPage) + 1 : this.totalItems % this.itemsPerPage;
          this.currentPage = page;
          this.loadingPage();
        }
        break;
      default:
        if (this.currentPage != e.target.innerText) {
          this.currentPage = e.target.innerText;
          this.loadingPage();
        }
        break;
    }
  }

  openModal(event, task) {
    event.preventDefault();
    if (task != null) {
      this.taskSelected = task;
    } else {
      this.taskSelected = null;
    }
    //this.taskEmmiter.emit(taskSelect);
    this.taskCommunicationService.announceTaskEdit(this.taskSelected);
    //{taskContract: this.taskSelected}
    this.modalService.open(TaskFormComponent, {taskSend: this.taskSelected});
  }

  loadingPage() {

    this.taskService.TaskAllPaginate(this.currentPage).then(resp => {
      if (resp.Header.status == this.appSettings.statusResponse.ok && resp.Data.data.length > 0) {

        this.taskList = resp.Data.data;
        this.currentPage = resp.Data.current_page;
        this.itemsPerPage = resp.Data.per_page;
        this.totalItems = resp.Data.total;

      }
      this.loadingTask = false;

    }).catch(error => {
      this.loadingTask = false;
    });
  }
  getDateDifferent(date){
    let dateNow = new Date();
    let dateTask = new Date(date);
    var timeDiff = Math.abs(dateNow.getTime() - dateTask.getTime());
    let day = Math.ceil(timeDiff / (1000 * 3600 * 24));
    if(day<3){
      return 'red'
    }else if(day>3 && day<5){
      return 'yellow';
    }else if(day>5 && day<10){
      return 'green'
    }else{
      return 'white';
    }
  }
}
