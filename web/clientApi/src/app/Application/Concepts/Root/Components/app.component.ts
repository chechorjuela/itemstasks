import {Component} from '@angular/core';
import {MzModalService} from 'ngx-materialize';
import {LocalStorageUtilsService} from "../../../../Domain/Helpers/Utils/LocalStorageUtils.Service";
import {AuthModel} from "../../../../Domain/Models/Auth/Auth.Model";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {AuthComunicationService} from "../../../../Domain/Services/Auth/AuthComunicationService";

@Component({
  providers: [AuthComunicationService],
  selector: 'app-root',
  templateUrl: '../View/app.component.html',
  styleUrls: ['../Css/app.component.css']
})
export class AppComponent {
  currentUser = false;
  private subscribeAuthCurrent: Subscription;

  constructor(
    private router: Router,
    private modalService: MzModalService,
    private localStoreUtils: LocalStorageUtilsService<AuthModel>,
    private authCurrentUserService: AuthComunicationService,
  ) {

    if (this.localStoreUtils.getObject('UserInfoStorage') != null) {
      this.currentUser = true;
    }
    this.subscribeAuthCurrent = this.authCurrentUserService.currentUserAnnounce$.subscribe(
      auth => {
        if (auth != null) {
          this.currentUser = true;
        } else {
          this.currentUser = false;
        }
      }
    )
  }

  logout() {
    this.localStoreUtils.deleteAuth();
    this.currentUser = false;
    this.router.navigate(['/'])
  }

}
