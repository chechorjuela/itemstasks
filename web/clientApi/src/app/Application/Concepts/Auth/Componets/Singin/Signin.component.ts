import {Component, OnInit} from "@angular/core";
import {AuthService} from "../../../../../Domain/Services/Auth/Auth.Service";
import {SigninContract} from "../../../../../Infrastructure/Contract/Auth/SigninContract";
import {LocalStorageUtilsService} from "../../../../../Domain/Helpers/Utils/LocalStorageUtils.Service";
import {AppSettings} from "../../../../../config/app.settings";
import {AuthModel} from "../../../../../Domain/Models/Auth/Auth.Model";
import {Router} from "@angular/router";
import {AuthComunicationService} from "../../../../../Domain/Services/Auth/AuthComunicationService";
import {MzToastService} from "ngx-materialize";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  providers: [AuthService, LocalStorageUtilsService],
  templateUrl: '../../View/Signin/Signin.component.html',
  selector: 'signin-app',
  styleUrls: []
})
export class SigninComponent implements OnInit {
  signIn: SigninContract = new SigninContract();
  loading: boolean = false;
  codeResponseSuccess: string;
  keyStringStore: string;
  userForm: FormGroup;

  ngOnInit(): void {
  }

  constructor(
    private authService: AuthService,
    private appSettings: AppSettings,
    private router: Router,
    private localStorageUtils: LocalStorageUtilsService<AuthModel>,
    private authComunnicationCurrentUser: AuthComunicationService,
    private toastService: MzToastService,
    private formBuilder: FormBuilder,
  ) {
    this.codeResponseSuccess = appSettings.statusResponse.ok;
    this.keyStringStore = appSettings.userKeyStore;
    this.buildForm();
  }

  buildForm() {
    this.userForm = this.formBuilder.group({
      password: [this.signIn.email, Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50)
      ])],
      email: [this.signIn.email, Validators.compose([
        Validators.required,
      ])]
    });
  }

  errorMessageResources = {
    password: {
      required: 'Firstname is required',
      minLength: 'Minimum 4 charaters',
      maxLength: 'Maximum 50 charaters'
    },
    email: {
      required: "Lastname is required"
    }
  };

  clickLogin(e) {
    e.preventDefault();
    if (this.userForm.valid) {
      this.loading = true;
      this.authService.Signin(this.signIn).then(resp => {
        if (resp.Header.status == this.codeResponseSuccess && resp.Data != null) {
          this.loading = false;
          this.localStorageUtils.deleteAuth();
          this.localStorageUtils.setItem(this.keyStringStore, resp.Data);
          this.authComunnicationCurrentUser.announceCurrentUSerAuth(resp.Data);
          this.toastService.show("Logeado", 4000, 'green', () => console.info("mostrar"));

          this.router.navigate(['admin/tasks']);
        }
      }).catch(error => {
        this.toastService.show("!ups a error", 4000, 'red', () => console.info("mostrar"));
      })
    }
  }
}
