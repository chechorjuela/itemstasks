import {Component, OnInit} from "@angular/core";
import {AuthService} from "../../../../../Domain/Services/Auth/Auth.Service";
import {UserSignUpContract} from "../../../../../Infrastructure/Contract/User/UserSignUpContract";
import {SigninContract} from "../../../../../Infrastructure/Contract/Auth/SigninContract";
import {Router} from "@angular/router";
import {AppSettings} from "../../../../../config/app.settings";
import {RoleEnum, RoleEnumModelArrayModel} from "../../../../../Domain/Helpers/Utils/RoleEnum";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MzToastService} from "ngx-materialize";

@Component({
  providers: [AuthService],
  templateUrl: '../../View/Signup/Singup.component.html',
  styleUrls: [],
  selector: 'signup-app'
})
export class SignupComponent implements OnInit {

  signUp: UserSignUpContract = new UserSignUpContract();
  codeResponseSuccess: string;
  loading: boolean = false;
  roles: Array<RoleEnum>;
  userForm: FormGroup;

  ngOnInit(): void {
  }

  constructor(
    public authService: AuthService,
    private router: Router,
    private appSettings: AppSettings,
    private formBuilder: FormBuilder,
    private toastService: MzToastService,
  ) {
    this.codeResponseSuccess = appSettings.statusResponse.ok;
    this.roles = new RoleEnumModelArrayModel().getListRoleEnumBase();
    this.buildForm();
  }

  errorMessageResources = {
    firstname: {
      required: 'Firstname is required',
      minLength: 'Minimum 4 charaters',
      maxLength: 'Maximum 50 charaters'
    },
    lastname: {
      required: "Lastname is required"
    },
    email: {
      required: "Email is required"
    },
    username: {
      required: "Username is required"
    },
    password: {
      required: "Password is required"
    },
    role: {
      required: "Role is required"
    }
  };

  buildForm() {
    this.userForm = this.formBuilder.group({
      firstname: [this.signUp.firstname, Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50)
      ])],
      lastname: [this.signUp.lastname, Validators.compose([
        Validators.required,
      ])],
      username: [this.signUp.username, Validators.compose([
        Validators.required,
      ])],
      password: [this.signUp.password, Validators.compose([
        Validators.required,
      ])],
      email: [this.signUp.email, Validators.compose([
        Validators.required,
      ])],
      role: [this.signUp.role, Validators.compose([
        Validators.required
      ])]
    });
  }

  clickSignUp(e) {
    this.loading = true;
    if (this.userForm.valid) {
      this.authService.Signup(this.signUp).then(resp => {
        if (resp.Header.status == this.codeResponseSuccess && resp.Data != null) {
          this.toastService.show("Usuario registrado", 4000, 'green', () => console.info("mostrar"));
          this.router.navigate(['auth/signin']);
        }
        this.loading = false;
      }).catch(error => {
        this.loading = false;
        this.toastService.show("!ups un error", 4000, 'red', () => console.info("mostrar"));
        console.info(error)
      })
    } else {
      this.loading = false;

    }

  }
}
