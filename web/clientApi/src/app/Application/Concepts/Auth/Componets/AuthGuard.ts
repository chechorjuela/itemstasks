import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable, Subscription} from "rxjs";
import {LocalStorageUtilsService} from "../../../../Domain/Helpers/Utils/LocalStorageUtils.Service";
import {AuthModel} from "../../../../Domain/Models/Auth/Auth.Model";
import {AuthService} from "../../../../Domain/Services/Auth/Auth.Service";
import {IAuthRepository} from "../../../../Infrastructure/Repositories/Auth/Auth.Repositroy";
import {AuthComunicationService} from "../../../../Domain/Services/Auth/AuthComunicationService";

@Injectable()
export class AuthGuard implements CanActivate {

  private authModel: AuthModel;
  private authComunicationService:AuthComunicationService;
  constructor(private router: Router,
              private localStoreUtils: LocalStorageUtilsService<AuthModel>,
              private loginRepository: IAuthRepository,
  ) {


  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (this.localStoreUtils.getObject('UserInfoStorage') == null) {
      return this.router.navigate(['/auth/signin'])
    }
    return this.checkAccess(route);
  }

  checkAccess(route: ActivatedRouteSnapshot): Promise<boolean> {
    this.authModel = this.localStoreUtils.getObject('UserInfoStorage');
    if (this.authModel) {
      this.loginRepository.GetUSer().then(resp => {
        if (resp.status == 503 || resp.Header.status == "fail") {
          this.localStoreUtils.deleteAuth();
          this.authComunicationService.announceCurrentUSerAuth(null);
          this.router.navigate(['/auth/signin'])
        }
      })
    }
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
}
