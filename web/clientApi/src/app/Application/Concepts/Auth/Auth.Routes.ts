import {Routes,RouterModule} from "@angular/router";
import { ModuleWithProviders } from '@angular/core';
import {SignupComponent} from "./Componets/Signup/Signup.component";
import {SigninComponent} from "./Componets/Singin/Signin.component";

export const AuthRoutes: Routes=[
  {path:'auth/signin', component:SigninComponent},
  {path:'auth/signup', component:SignupComponent},
  ];
