export class RoleEnum {
  Value: string;
  Name: string;
}
export class RoleEnumModelArrayModel {
  getListRoleEnumBase():Array<RoleEnum>{
    return [
      { Name : "Admin", Value: "1" },
      { Name : "User", Value : "2" }
    ];
  }
}
