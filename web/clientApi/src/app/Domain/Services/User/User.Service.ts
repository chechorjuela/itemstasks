import {Injectable} from "@angular/core";
import {ResponseApiModel} from "../../Models/Common/ResponseApiModel";
import {TaskModel} from "../../Models/Task/TaskModel";
import {TaskContract} from "../../../Infrastructure/Contract/Task/TaskContract";
import {ITaskRepository} from "../../../Infrastructure/Repositories/Task/Task.Repository";
import {ResponseData} from "../../Models/Common/ResponseData";
import {UserModel} from "../../Models/Users/UserModel";
import {IUserRepository} from "../../../Infrastructure/Repositories/User/User.Repository";

@Injectable()
export abstract class IUserService {
  abstract getAllUsers():Promise<ResponseApiModel<ResponseData<UserModel>>>;

}

@Injectable()
export class UserService implements IUserService{
  constructor(private userRepository:IUserRepository){

  }

  getAllUsers(): Promise<ResponseApiModel<ResponseData<UserModel>>> {
    return this.userRepository.getAllUser().then(resp=>{
      return resp;
    })
  }


}
