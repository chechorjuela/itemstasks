import {Injectable} from "@angular/core";
import {ResponseApiModel} from "../../Models/Common/ResponseApiModel";
import {TaskModel} from "../../Models/Task/TaskModel";
import {TaskContract} from "../../../Infrastructure/Contract/Task/TaskContract";
import {ITaskRepository} from "../../../Infrastructure/Repositories/Task/Task.Repository";
import {ResponseData} from "../../Models/Common/ResponseData";

@Injectable()
export abstract class ITaskService {
  abstract TaskAllPaginate(page:number):Promise<ResponseApiModel<ResponseData<TaskModel>>>;
  abstract saveItemsTask(task:TaskContract):Promise<ResponseApiModel<TaskModel>>;
  abstract deleteItemTask(id:number):Promise<ResponseApiModel<TaskModel>>;
  abstract updateItemTask(task:TaskContract,id:number):Promise<ResponseApiModel<TaskModel>>;
}

@Injectable()
export class TaskService implements ITaskService{
  constructor(public taskRepository:ITaskRepository){

  }
  TaskAllPaginate(page:number): Promise<ResponseApiModel<ResponseData<TaskModel>>> {
    return this.taskRepository.getAllItemsPaginate(page).then(resp=>{
      let taskContractRepository=[];
      resp.Data.data.map((task,index)=>{
        let indexDate = task.date_expiration.indexOf(" ");
        let dateFormat = task.date_expiration.substring(0,indexDate).replace("-","/").replace("-","/");
        task.date_expiration = dateFormat;
        taskContractRepository.push(task);
      });
      if(taskContractRepository.length>0){
        resp.Data.data = taskContractRepository;
      }
      return resp;
    })
  }

  saveItemsTask(task: TaskContract): Promise<ResponseApiModel<TaskModel>> {
    return this.taskRepository.saveItemsTask(task).then(resp=>{
      return resp;
    })
  }

  deleteItemTask(id: number): Promise<ResponseApiModel<TaskModel>> {
    return this.taskRepository.deleteItemTask(id).then(resp=>{
      return resp;
    })
  }

  updateItemTask(task: TaskContract,id:number): Promise<ResponseApiModel<TaskModel>> {
    return this.taskRepository.updateItemTask(task,id).then(resp=>{
      return resp;
    })
  }

}
