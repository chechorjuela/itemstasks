import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {TaskContract} from "../../../Infrastructure/Contract/Task/TaskContract";
import {TaskModel} from "../../Models/Task/TaskModel";

@Injectable()
export class TaskComunicationService {
  private sendAnnounceTaskEdit = new Subject<TaskContract>();
  sendAnnounceTaskEdit$ = this.sendAnnounceTaskEdit.asObservable();
  announceTaskEdit(task:TaskContract){
    this.sendAnnounceTaskEdit.next(task);
  }

  private sendAnnounceTaskPush = new Subject<TaskModel>();
  sendAnnounceTaskPush$ = this.sendAnnounceTaskPush.asObservable();
  annouceTaskPush(task:TaskModel) {
    this.sendAnnounceTaskPush.next(task);
  }
}
