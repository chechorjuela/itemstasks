import {Inject, Injectable} from "@angular/core";
import {AuthRepositroy, IAuthRepository} from "../../../Infrastructure/Repositories/Auth/Auth.Repositroy";
import {SigninContract} from "../../../Infrastructure/Contract/Auth/SigninContract";
import {ResponseApiModel} from "../../Models/Common/ResponseApiModel";
import {AuthModel} from "../../Models/Auth/Auth.Model";
import {UserSignUpContract} from "../../../Infrastructure/Contract/User/UserSignUpContract";
import {UserModel} from "../../Models/Users/UserModel";

@Injectable()
export abstract class IAuthService {
  abstract Signin(signin:SigninContract):Promise<ResponseApiModel<AuthModel>>;
  abstract Signup(signup:UserSignUpContract):Promise<ResponseApiModel<UserModel>>;
  abstract Logout();
}

@Injectable()
export class AuthService implements IAuthService{
  constructor(public authRepository:IAuthRepository){

  }
  Logout() {
  }

  Signin(signin:SigninContract):Promise<ResponseApiModel<AuthModel>> {
    return this.authRepository.SignIn(signin).then(data=>{
      return data;
    });

  }

  Signup(signup: UserSignUpContract): Promise<ResponseApiModel<UserModel>> {
    return this.authRepository.SignUp(signup).then(data=>{

      return data;
    });
  }



}
