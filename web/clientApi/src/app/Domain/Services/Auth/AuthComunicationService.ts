import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {TaskContract} from "../../../Infrastructure/Contract/Task/TaskContract";
import {TaskModel} from "../../Models/Task/TaskModel";
import {AuthModel} from "../../Models/Auth/Auth.Model";

@Injectable()
export class AuthComunicationService {
  private currentUserAnnounce = new Subject<AuthModel>();
  currentUserAnnounce$ = this.currentUserAnnounce.asObservable();
  announceCurrentUSerAuth(auth:AuthModel){
    this.currentUserAnnounce.next(auth);
  }

}
