export class UserModel {
  username:string;
  firstname:string;
  lastname: string;
  email:string;
  role: string;
  updated_at?: Date;
  created_at?: Date;
  id: number;
}
