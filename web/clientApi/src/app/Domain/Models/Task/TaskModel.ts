export class TaskModel {
  id:number;
  name:string;
  priority_id:string;
  date_expiration:string;
  created_at:Date;
  updated_at:string;
  user_id:number;
}
