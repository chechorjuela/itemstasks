export class AuthModel {
  id:number;
  firstname:string;
  lastname:string;
  username:string;
  email:string;
  role:string;
  created_at:Date;
  updated_at:Date;
  token:string;
}
