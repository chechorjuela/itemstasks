<?php
/**
 * Created by IntelliJ IDEA.
 * User: chechorjuela
 * Date: 3/03/19
 * Time: 07:25 AM
 */

namespace App\Infrastructure\Service;


class ResponseApi
{
    private $response = ['Header' => ['status' => '', 'message' => ''], 'Data' => []];
    private $status = 'success';

    public function returnResponse($data, $code, $message = null, $status = null)
    {
        if ($status != null)
            $this->status = $status;

        $this->response['Header']['status'] = $this->status;
        $this->response['Data'] = $data;
        if (isset($message)):
            $this->response['Header']['message'] = $message;
        endif;
        return response()->json($this->response, $code);
    }
}
