<?php
/**
 * Created by IntelliJ IDEA.
 * User: chechorjuela
 * Date: 3/03/19
 * Time: 08:04 AM
 */

namespace App\Infrastructure\Service;


use App\Domain\Model\Task;
use App\Domain\Repository\ITaskRepository;

class TaskService
{
    private $ITaskRepository;

    public function __construct(ITaskRepository $ITaskRepository)
    {
        $this->ITaskRepository = $ITaskRepository;
    }

    /**
     * @param Task $task
     * @return Task|null
     */
    public function addTask(Task $task):?Task{
        $task->setUserId(auth()->user()->getAuthIdentifier());
        return $this->ITaskRepository->create($task);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function removeTask(int $id):bool{
        return $this->ITaskRepository->delete($id);
    }

    /**
     * @param Task $task
     * @return Task|null
     */
    public function updateTask(int $taskId,array $data):?Task{

        return $this->ITaskRepository->update($taskId,$data);
    }

    /**
     * @return array|null
     */
    public function getAllItems():?array{
        return $this->ITaskRepository->all();
    }

    public function findById(int $id):?Task{
        return $this->ITaskRepository->findById($id);
    }
}
