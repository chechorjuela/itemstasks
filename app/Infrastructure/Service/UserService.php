<?php
/**
 * Created by IntelliJ IDEA.
 * User: chechorjuela
 * Date: 2/03/19
 * Time: 06:50 PM
 */

namespace App\Infrastructure\Service;


use App\Domain\Model\User;
use App\Domain\Repository\IuserRepositoryInterface;
use JWTAuth;

class UserService
{
    /**
     * @var IuserRepositoryInterface
     */
    private $IUserRepository;

    /**
     * UserService constructor.
     * @param IuserRepositoryInterface $IuserRepository
     */
    public function __construct(IuserRepositoryInterface $IUserRepository)
    {
        $this->IUserRepository = $IUserRepository;
    }

    /**
     * @param $id
     * @return \App\Domain\Model\User|mixed|null
     */
    public function findUserById():?User{
        $user = JWTAuth::parseToken()->authenticate();
       return $this->IUserRepository->findById($user->id);
    }

    public function createUser(User $user):?User{
        return $this->IUserRepository->save($user);
    }

    public function loginUser(array $credentials):?User{
        return $this->IUserRepository->login($credentials['email'],$credentials['password']);
    }

    public function getUsersAll():?array {
        return$this->IUserRepository->getAllUser();
    }
}
