<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'date_expiration'=>'required|date_format:Y/m/d',
            'priority'=>'required',
            'user_from' => 'required'
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'=>'El :attribute es requreido',
            'date_expiration.date_format'=>'La fecha de expiracion es incorrecta',
            'priority.required'=>'La proridad es obligatoria',
            'user_from.required' => 'El :attribute es requerido'

        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator) {
        $json = [];
        $json["code"]=200;
        $json["data"]=$validator->errors();
        throw new HttpResponseException(response()->json($json, 422));
    }
}
