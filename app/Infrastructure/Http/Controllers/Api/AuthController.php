<?php

namespace App\Infrastructure\Http\Controllers\Api;

use App\Domain\Model\User;
use App\Infrastructure\Requests\LoginRequest;
use App\Infrastructure\Requests\UserRequest;
use App\Infrastructure\Service\ResponseApi;
use App\Infrastructure\Service\UserService;
use Illuminate\Http\Request;
use App\Infrastructure\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * @var
     */
    private $userSerive;

    private $responseApi;

    public function __construct(UserService $userService, ResponseApi $responseApi)
    {
        $this->responseApi = $responseApi;
        $this->userSerive = $userService;
        $this->middleware('jwtAuthenticator', ['except' => ['signup', 'signin']]);
    }

    public function profile()
    {
        try {
            return $this->responseApi->returnResponse($this->userSerive->findUserById(), 201);
        } catch (\Exception $e) {
            return $this->responseApi->returnResponse(null, 400, $e->getMessage());
        }
//
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signup(UserRequest $request)
    {
        try {
            if($request->validated()){
                $user = new User();
                $user->setPassword(bcrypt($request->get('password')));
                $user->setFirstname($request->get('firstname'));
                $user->setLastname($request->get('lastname'));
                $user->setRole($request->get('role'));
                $user->setEmail($request->get('email'));
                $user->setUsername($request->get('username'));

                return $this->responseApi->returnResponse($this->userSerive->createUser($user), 201);
            }

        } catch (\Exception $e) {
            return $this->responseApi->returnResponse(null, 400, $e->getMessage(),"failed");
        }

    }

    public function signin(LoginRequest $request)
    {

        try {
            if($request->validated()){
                $credentials = request(['email', 'password']);
                return $this->responseApi->returnResponse($this->userSerive->loginUser($credentials), 201);
            }
         } catch (\Exception $e) {
            return $this->responseApi->returnResponse(null, 400, $e->getMessage());
        }


    }
}
