<?php

namespace App\Infrastructure\Http\Controllers\Api;

use App\Domain\Model\Task;
use App\Http\Requests\TaskRequest as TaskRequestAlias;
use App\Infrastructure\Http\Controllers\Controller;
use App\Infrastructure\Service\ResponseApi;
use App\Infrastructure\Service\TaskService;
use Illuminate\Http\Request;


class TaskController extends Controller
{
    /**
     * @var TaskService
     */
    private $taskService;
    /**
     * @var ResponseApi
     */
    private $responseApi;

    /**
     * TaskController constructor.
     * @param TaskService $taskService
     * @param ResponseApi $responseApi
     */
    public function __construct(TaskService $taskService, ResponseApi $responseApi)
    {
        $this->middleware('jwtAuthenticator');
        $this->taskService = $taskService;
        $this->responseApi = $responseApi;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return $this->responseApi->returnResponse($this->taskService->getAllItems(), 201);

        } catch (\Exception $e) {
            return $this->responseApi->returnResponse(null, 400, $e->getMessage(), "failed");
        }
    }

    public function store(TaskRequestAlias $request)
    {
        try {
            if ($request->validated()) {
                $task = new Task();
                $task->setName($request->get('name'));
                $task->setPriority($request->get('priority'));
                $task->setDateExpiration($request->get('date_expiration'));
                $task->setUserFrom($request->get('user_from'));
                return $this->responseApi->returnResponse($this->taskService->addTask($task), 201);
            }

        } catch (\Exception $e) {
            return $this->responseApi->returnResponse(null, 400, $e->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $this->responseApi->returnResponse($this->taskService->findById($id), 201);

        } catch (\Exception $e) {
            return $this->responseApi->returnResponse(null, 400, $e->getMessage());
        }
    }

    public function update(TaskRequestAlias $request, $id)
    {
        try {
            if ($request->validated()) {
                $data = $request->all();
                return $this->responseApi->returnResponse($this->taskService->updateTask($id, $data), 201);
            }
        } catch (\Exception $e) {
            return $this->responseApi->returnResponse(null, 400, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = [];
            $message = '';
            if ($delete = $this->taskService->removeTask($id)) {
                $data['id'] = $id;
                $data['action'] = $delete;
                $message = 'Eliminado';
            }
            return $this->responseApi->returnResponse($data, 201, $message);
        } catch (\Exception $e) {
            return $this->responseApi->returnResponse(null, 400, $e->getMessage());
        }

    }
}
