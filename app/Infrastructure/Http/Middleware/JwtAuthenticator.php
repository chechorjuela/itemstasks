<?php

namespace App\Infrastructure\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use App\Infrastructure\Service\ResponseApi;
class JwtAuthenticator
{
    private $responseApi;

    public function __construct(ResponseApi $responseApi)
    {
        $this->responseApi = $responseApi;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->session()->regenerate();
        $response = ['status'=>'fail','data'=>[]];
        $code = 400;
        try {
            if (!!$user = JWTAuth::parseToken()->authenticate()) {
                return $next($request);
            }
        } catch (JWTException $e) {
       /*     $response['data'] = [
                'data'=>'',
                'code'=>$e->getLine(),
                'message'=>$e->getMessage()
            ];
            */
            //return response()->json($response, 201);

        }
        return $this->responseApi->returnResponse($response["data"],503,$e->getMessage(),'fail');
        //return response()->json($response, $code);
    }
}
