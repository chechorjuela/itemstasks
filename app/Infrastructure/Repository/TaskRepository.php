<?php
/**
 * Created by IntelliJ IDEA.
 * User: chechorjuela
 * Date: 3/03/19
 * Time: 08:02 AM
 */

namespace App\Infrastructure\Repository;


use App\Domain\Model\Task;
use App\Domain\Repository\ITaskRepository;
use phpDocumentor\Reflection\Types\Integer;
use Tymon\JWTAuth\Facades\JWTAuth;

class TaskRepository implements ITaskRepository
{

    /**
     * @param Integer $id
     * @return Task|null
     */
    public function findById(int $id): ?Task
    {
        // TODO: Implement findById() method.
        return Task::find($id);
    }

    /**
     * @param Task $task
     * @return Task|null
     */
    public function create(Task $task): ?Task
    {
        return Task::create($task->dataSerialize());
    }

    /**
     * @param Task $task
     * @return Task|null
     */
    public function update(int $taskId, array $data): ?Task
    {
        $task = Task::find($taskId);
        $task->fill($data);
        $task->save();
        return $task;
        //return $task->save($task);
    }

    /**
     * @param Integer $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $task = Task::find($id);
        if ($task->delete()) {
            return true;
        }
        return false;
    }

    /**
     * @return array|null
     */
    public function all(): ?array
    {
        $whereCondtionals=[];
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->role != 'Admin') {
            $whereCondtionals=['user_from'=>$user->id];
        }
        return Task::where($whereCondtionals)->paginate(10)->toArray();
    }
}
