<?php

namespace App\Domain\Model;

use Carbon\Carbon;
use Carbon\Traits\Date;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Integer;

class Task extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'priority', 'date_expiration', 'user_id', 'user_from'
    ];
    /**
     * @var
     */
    protected $id;
    /**
     * @var
     */
    protected $name;
    /**
     * @var
     */
    protected $date_expiration;
    /**
     * @var
     */
    protected $priority;

    protected $userId;

    protected $user_from;

    /**
     * @var
     */
    protected $createAt;
    /**
     * @var
     */
    protected $updateAt;

    /**
     * @return mixed
     */
    public function getId(): Integer
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDateExpiration(): ?string
    {
        return $this->date_expiration;
    }

    /**
     * @param mixed $date_expiration
     */
    public function setDateExpiration($date_expiration): void
    {
        $this->date_expiration = Carbon::parse($date_expiration);
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return mixed
     */
    public function getCreateAt(): ?string
    {
        return $this->createAt;
    }

    /**
     * @param mixed $createAt
     */
    public function setCreateAt($createAt): void
    {
        $this->createAt = $createAt;
    }

    /**
     * @return mixed
     */
    public function getUpdateAt(): ?\DateTime
    {
        return $this->updateAt;
    }

    /**
     * @param mixed $updateAt
     */
    public function setUpdateAt($updateAt): void
    {
        $this->updateAt = $updateAt;
    }

    /**
     * @return mixed
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getUserFrom(): int
    {
        return $this->user_from;
    }

    /**
     * @param int $userId
     */
    public function setUserFrom(int $userId): void
    {
        $this->user_from = $userId;
    }

    /**
     * @return array
     */
    public function dataSerialize(): array
    {
        $dataArray = [
            'name' => $this->getName(),
            'date_expiration' => $this->getDateExpiration(),
            'priority' => $this->getPriority(),
            'createAt' => $this->getCreateAt(),
            'updateAt' => $this->getUpdateAt(),
            'user_id' => $this->getUserId(),
            'user_from' => $this->getUserFrom()
        ];
        return $dataArray;
    }


}
