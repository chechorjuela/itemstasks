<?php
/**
 * Created by IntelliJ IDEA.
 * User: chechorjuela
 * Date: 3/03/19
 * Time: 07:58 AM
 */

namespace App\Domain\Repository;


use App\Domain\Model\Task;
use App\Domain\Model\User;
use phpDocumentor\Reflection\Types\Integer;

interface ITaskRepository
{
    /**
     * @param Integer $id
     * @return Task|null
     */
    public function findById(int $id):?Task;

    /**
     * @param Task $task
     * @return Task|null
     */
    public function create(Task $task):?Task;

    /**
     * @param int $taskId
     * @param array $data
     * @return Task|null
     */
    public function update(int $taskId,array $data):?Task;

    /**
     * @param Integer $id
     * @return bool
     */
    public function delete(int $id):bool;

    /**
     * @return array|null
     */
    public function all():?array;
}
